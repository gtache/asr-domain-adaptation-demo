from flask import jsonify, request, render_template
import requests
import config as conf
from . import domain_adaptation

results = []
with open('domain_adaptation/static/asset/results.txt') as f:
    for line in f.readlines():
        results.append(line[:-1].split("/"))


@domain_adaptation.route('')
def showDomainAdaptationPage():
    return render_template('domain_adaptation.html')


@domain_adaptation.route('', methods=['POST'])
def submitDomainAdaptation():
    parameters = request.get_json(force=True)
    result = results[int(parameters['sampleId'])]
    resultdict = {'gt': result[0], 'tedlium': result[1], 'tedlium_adapted': result[2]}
    return jsonify(resultdict)
