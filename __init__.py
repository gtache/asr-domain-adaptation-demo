from flask import Blueprint

domain_adaptation = Blueprint('domain_adaptation', __name__, template_folder='templates', static_folder='static')

from . import domain_adaptation_controller
