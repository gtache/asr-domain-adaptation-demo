/* -------------  Audio player  ------------- */

// Audio Samples
const audioSamples = [
    "1.wav",
    "2.wav",
    "3.wav",
    "4.wav",
    "5.wav",
    "6.wav",
    "7.wav"
];

// Audio Samples
const uttId = [
    0,
    1,
    2,
    3,
    4,
    5,
    6
];

// Audio Instance
var wavesurfer = WaveSurfer.create({
    container: "#waveform",
    waveColor: "#ADADAD",
    scrollParent: true,
    progressColor: "#0851DA",
    barHeight: 2,
    height: 41,
    width: 100,
    hideScrollbar: true,
    audioRate: 1
});

// Load the audio sample
var loadAudioSample = function (id) {
    $("#stop-btn").addClass("display-none");
    $("#play-btn").removeClass("display-none");

    audioSample = audioSamples[id];
    let audioPathFile = "domain_adaptation/static/asset/" + audioSample;
    wavesurfer.load(audioPathFile);
    showCurrentRate();

    let audioDuration = showAudioDuration();
    document.getElementById("duration-time").innerHTML = audioDuration;

    let currentTime = showCurrentTime();
    document.getElementById("current-time").innerHTML = currentTime;
};

/* ---------- Audio options ---------- */

// Play/Pause
$(".btn-container").on("click", function () {
    wavesurfer.playPause();
    let play_btn = $("#play-btn");
    let stop_btn = $("#stop-btn");
    if (play_btn.hasClass("display-none")) {
        play_btn.removeClass("display-none");
    } else {
        play_btn.addClass("display-none");
    }
    if (stop_btn.hasClass("display-none")) {
        stop_btn.removeClass("display-none");
    } else {
        stop_btn.addClass("display-none");
    }
});

//Audio current time
wavesurfer.on("play", function () {
    setInterval(function () {
        let currentTime = showCurrentTime();
        document.getElementById("current-time").innerHTML = currentTime;
    }, 100);
});

// Reset audio at the start
wavesurfer.on("finish", function () {
    $("#stop-btn").addClass("display-none");
    $("#play-btn").removeClass("display-none");
    wavesurfer.stop();
});

// Set the audio speed rate
$(".rate-option").on("click", function () {
    let index = wavesurfer.getPlaybackRate();
    const rates = [0.8, 1, 1.2];
    if (this.id === "plus") {
        wavesurfer.setPlaybackRate(rates[index + 1]);
    } else if (this.id === "minus") {
        wavesurfer.setPlaybackRate(rates[index - 1]);
    }
    let currentRate = wavesurfer.getPlaybackRate();
    $("#audio-speed-rate").text(currentRate + "x");
});

// Navigation for the samples
$(".sample-label span").on("click", function () {
    loadAudioSample(this.id);
    $(".sample-label span").removeClass("active");
    $("#" + this.id).addClass("active");
    submit()

});

function clearResults() {
    let arr = ["#def_pred", "#def_lm_pred", "#cf_pred"];
    arr.forEach(x => {
        $(x).text("");
    });
}

function showResults() {
    let arr = ["#def_pred", "#def_lm_pred", "#cf_pred"];
    arr.forEach(x => {
        $(x).removeClass("display-none");
    });
}

// Restart the audio sample (return to the start)
$("#go-start").on("click", function () {
    wavesurfer.stop();
    $("#stop-btn").addClass("display-none");
    $("#play-btn").removeClass("display-none");
});

// Finish the audio sample
$("#go-end").on("click", function () {
    wavesurfer.skipForward(10);
    $("#stop-btn").addClass("display-none");
    $("#play-btn").removeClass("display-none");
});

function submit() {
    let currentSampleId = $(".active").attr("id");
    let url = "/domain_adaptation";
    let data = {sampleId: uttId[currentSampleId]};
    console.log("Sent id " + currentSampleId);
    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        data: JSON.stringify(data),
        success: function (data) {
            let groundtruth = data["gt"];
            let result_tedlium = data["tedlium"];
            let result_adapted = data["tedlium_adapted"];
            let tedlium_pred = $("#tedlium_pred");
            let tedlium_adapted_pred = $("#tedlium_adapted_pred");
            if (currentSampleId < 4) {
                tedlium_pred.removeClass("int-red");
                tedlium_pred.addClass("int-green");
                tedlium_adapted_pred.removeClass("int-green");
                tedlium_adapted_pred.addClass("int-red");
            } else {
                tedlium_adapted_pred.removeClass("int-red");
                tedlium_adapted_pred.addClass("int-green");
                tedlium_pred.removeClass("int-green");
                tedlium_pred.addClass("int-red");
            }
            /*$("#loader_def").removeClass("display-none");
            $("#loader_def_lm").removeClass("display-none");
            $("#loader_cf").removeClass("display-none");
            setTimeout(function () {
                $("#loader_def").addClass("display-none");
                $("#def_pred").text(result_default).show();
            }, 3000 + Math.random() * 3000);
            setTimeout(function () {
                $("#loader_def_lm").addClass("display-none");
                $("#def_lm_pred").text(result_lm).show();
            }, 3000 + Math.random() * 3000);
            setTimeout(function () {
                $("#loader_cf").addClass("display-none");
                $("#cf_pred").text(result_cf).show();
            }, 3000 + Math.random() * 3000);
            setTimeout(function () {
                $("#decode-btn").removeClass("disabled");
            }, 6000);*/
            $("#groundtruth").text(groundtruth).show();
            tedlium_pred.text(result_tedlium).show();
            tedlium_adapted_pred.text(result_adapted).show();
            $("#decode-btn").removeClass("disabled");
        }
    });
}

$("#decode-btn").on("click", function () {
    submit()
    $("#decode-btn").addClass("disabled");
    clearResults();
});

$(document).ready(function () {
    $("#audio-speed-rate").text(wavesurfer.audioRate);
    loadAudioSample(0);
    submit()
});

// Functions definition

function showAudioDuration() {
    let audioDuration = wavesurfer.getDuration();
    let minutes = "0" + Math.floor(audioDuration / 60);
    let seconds = "0" + Math.floor(audioDuration - minutes * 60);
    let formattedDuration = minutes.substr(-1) + ":" + seconds.substr(-2);
    return formattedDuration;
}

function showCurrentTime() {
    let time = wavesurfer.getCurrentTime();
    let minutes = "0" + Math.floor(time / 60);
    let seconds = "0" + Math.floor(time - minutes * 60);
    let currentTime = minutes.substr(-1) + ":" + seconds.substr(-2);
    return currentTime;
}

function showCurrentRate() {
    let audioRate = wavesurfer.getPlaybackRate();
    $("#audio-speed-rate").text(audioRate + "x");
}
